﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanRecognition
{
    public partial class Input : Form
    {
        public Input()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtStr.Text.Trim().Length!=45)
            {
                MessageBox.Show("请输入有效45位条形码", "警告");
                txtStr.Focus();
            }
            else
            {
                Form1.str = txtStr.Text.Trim();
                Form1.flag = true;           
                this.Dispose();
            }
        }
    }
}
