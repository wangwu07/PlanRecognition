﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanRecognition
{
    public class Model
    {
        public int Id { get; set; }
        public string Numbers { get; set; }
        public string Heat { get; set; }
        public string Steels { get; set; }
        public string Thick { get; set; }
        public string Widths { get; set; }
        public string Lengths { get; set; }
        public DateTime Datetime { get; set; }

    }
}
