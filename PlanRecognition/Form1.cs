﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanRecognition
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //public Form1(string[] args)
        //{
        //    InitializeComponent();
        //    if (args.Length > 0)
        //    {
        //        str = args[0];
        //    }
        //    else
        //    {
        //        MessageBox.Show("扫描失败,请手动输入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        Input input = new Input();
        //        input.ShowDialog();
        //    }
        //    this.Load += new EventHandler(Form1_Load);
        //}
        public static string str;                  //人工录入或者扫描条形码
        public static Model model = new Model();   //扫描模型信息
        public static Model modelp = new Model();  //计划模型信息
        public static DataTable dt;                //获取计划表
        public static DataTable dtP;                //生产计划表
        public static bool flag = true;                //条形码输入完毕
        public static string ServerIP= "127.0.0.1";                //服务器IP
        public static int Ports= "21000";                //端口号


        private void Form1_Load(object sender, EventArgs e)
        {

            //生产表初始化
            dtP = new DataTable();
            dtP.Columns.Add("Id", typeof(Int32)); //Id
            dtP.Columns.Add("Numbers", typeof(String)); //名称
            dtP.Columns.Add("Heat", typeof(String)); //名称
            dtP.Columns.Add("Steels", typeof(String)); //操作人
            dtP.Columns.Add("Thick", typeof(String)); //操作时间
            dtP.Columns.Add("Widths", typeof(String)); //操作时间
            dtP.Columns.Add("Lengths", typeof(String)); //操作时间
            dtP.Columns.Add("Datetime", typeof(DateTime)); //操作时间
            timer1.Interval = 10000;
           // timer1.Start();

        }
        public static DataTable GetPlanList()
        {
            DataTable dt = new DataTable();
            try
            {
                string connString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=180.166.73.51)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=EMSDB1)));Persist Security Info=True;User ID=emsuser1;Password=Ems168;";
                OracleConnection conn = new OracleConnection(connString);
                conn.Open();
                string sqlStr = "select * from test";
                OracleCommand cmd = new OracleCommand(sqlStr, conn);
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                da.Fill(dt);
                cmd.Dispose();
                conn.Dispose();
                return dt;
            }
            catch (Exception ee)
            {
                throw ee; //如果有错误，输出错误信息
            }
        }

        //扫描条形码赋值
        public void setValue()
        {
            model.Numbers = str.Substring(0, 12);
            model.Heat = str.Substring(12, 9);
            model.Steels = str.Substring(21, 5);
            model.Thick = str.Substring(26, 3);
            model.Widths = str.Substring(29, 4);
            model.Lengths = str.Substring(33, 4);
            model.Datetime = DateTime.ParseExact(str.Substring(37, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
        }

        //判断是否在计划表中
        public bool IsInPlan()
        {
            bool result = false;
            dt = GetPlanList();
            if (dt.Rows.Count < 0)
            {
                MessageBox.Show("提示", "计划表获取失败！");
                this.Dispose();
            }
            //dataGridView1.DataSource = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Numbers = dt.Rows[i]["Numbers"].ToString();
                //查询数据库是否存在计划 存在计划则赋值显示在计划表
                if (model.Numbers == Numbers)
                {
                    result = true;
                    modelp.Numbers = dt.Rows[i]["Numbers"].ToString();
                    modelp.Heat = dt.Rows[i]["Heat"].ToString();
                    modelp.Steels = dt.Rows[i]["Steels"].ToString();
                    modelp.Thick = dt.Rows[i]["Thick"].ToString();
                    modelp.Widths = dt.Rows[i]["Widths"].ToString();
                    modelp.Lengths = dt.Rows[i]["Lengths"].ToString();
                    modelp.Datetime = Convert.ToDateTime(dt.Rows[i]["Datetime"].ToString());

                    txt_Result.ForeColor = Color.Green;
                    txt_Result.Text = "匹配成功";

                    //紧停非选中 背景色
                    c_stop.Enabled = false;
                    c_stop.Checked = false;
                    groupBox6.ForeColor = Color.PowderBlue;

                    //添加计划面板不可用
                    groupBox5.Enabled = false;
                    addPlanTable();
                    dt.Rows.RemoveAt(i);
                    break;
                }
                else
                {
                    //匹配信息
                    txt_Result.ForeColor = Color.Red;
                    txt_Result.Text = "匹配失败";
                    c_stop.Enabled = true;

                    //紧停选中 背景色
                    c_stop.Checked = true;
                    groupBox6.BackColor = Color.Red;

                    //添加计划面板可用
                    groupBox5.Enabled = true;

                    //添加计划表赋值
                    addPlanValue();
                }
            }
            return result;
        }

        //添加计划 赋值
        public void addPlanValue()
        {
            txtNumber3.Text = model.Numbers;
            txt_heat3.Text = model.Heat;
            txt_steel3.Text = model.Steels;
            txt_thick3.Text = model.Thick;
            txt_width3.Text = model.Widths;
            txt_length3.Text = model.Lengths;
            txt_date3.Text = model.Datetime.ToShortDateString();
        }

        //生产计划表添加信息
        public void addPlanTable()
        {
            DataRow row = dtP.NewRow();
            row["Id"] = dt.Rows.Count + 1;
            row["Numbers"] = model.Numbers;
            row["Heat"] = model.Heat;
            row["Steels"] = model.Steels;
            row["Thick"] = model.Thick;
            row["Widths"] = model.Widths;
            row["Lengths"] = model.Lengths;
            row["Datetime"] = model.Datetime;
            dtP.Rows.Add(row);
            dataGridView1.DataSource = dtP;
            dataGridView1.Columns[0].Width = 60;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 100;
            dataGridView1.Columns[3].Width = 100;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
            dataGridView1.Columns[6].Width = 100;
            dataGridView1.Columns[7].Width = 100;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("提示", "确定要添加计划信息吗?");
            if (res == DialogResult.OK)
            {
                addPlanTable();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (flag == true)
            {
                DialogResult re = MessageBox.Show("扫描失败,请手动输入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (re == DialogResult.OK)
                {
                    flag = false;
                    Input input = new Input();
                    input.ShowDialog();
                }
                setValue();
                bool res = IsInPlan();
                if (res == false)
                {
                    flag = false;
                }
                else
                {
                    txtNumber1.Text = model.Numbers;
                    txt_heat1.Text = model.Heat;
                    txt_steel1.Text = model.Steels;
                    txt_thick1.Text = model.Thick;
                    txt_width1.Text = model.Widths;
                    txt_length1.Text = model.Lengths;
                    txt_date1.Text = model.Datetime.ToShortDateString();

                    txtNumber2.Text = modelp.Numbers;
                    txt_heat2.Text = modelp.Heat;
                    txt_steel2.Text = modelp.Steels;
                    txt_thick2.Text = modelp.Thick;
                    txt_width2.Text = modelp.Widths;
                    txt_length2.Text = modelp.Lengths;
                    txt_date2.Text = modelp.Datetime.ToShortDateString();
                }
            }
        }

        private void c_stop_CheckedChanged(object sender, EventArgs e)
        {
            flag = true;

        }

        private void btn_ConnetServer_Click(object sender, EventArgs e)
        {
            ServerConfig sc = new ServerConfig();
            sc.ShowDialog();
        }
    }
}
