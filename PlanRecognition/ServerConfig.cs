﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlanRecognition
{
    public partial class ServerConfig : Form
    {
        public ServerConfig()
        {
            InitializeComponent();
        }

        private void btn_Yes_Click(object sender, EventArgs e)
        {
            if (IsIp(txt_IP.Text))
            {
                if (IsPort(txt_Port.Text))
                {
                    Form1.ServerIP = txt_IP.Text.Trim();
                    Form1.Ports =Convert.ToInt32(txt_Port.Text.Trim());
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("请输入有效端口号", "提示");
                    txt_Port.Focus();
                }

            }
            else
            {
                MessageBox.Show("请输入有效IP", "提示");
                txt_IP.Focus();
            }

        }
        public static bool IsIp(string ip)
        {
            if (string.IsNullOrEmpty(ip))
            {
                return false;
            }
            ip = ip.Trim();
            string pattern = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";
            return Regex.IsMatch(ip, pattern);
        }
        public static bool IsPort(string port)
        {
            if (string.IsNullOrEmpty(port))
            {
                return false;
            }
            port = port.Trim();
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[-]?\d+[.]?\d*$");
            return reg1.IsMatch(port);

        }
        private void btn_Esc_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
